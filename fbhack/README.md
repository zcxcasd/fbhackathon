ducking-dubstep
===============

Repository for the 2013 Summer Hackathon at Facebook Seattle.

## Install

1. First install [NodeJS](http://nodejs.org/)
2. Clone the repository
3. Run `npm install`

## Running

1. Install [Nodemon](https://github.com/remy/nodemon)
2. Install [Grunt-cli](http://gruntjs.com/getting-started)
3. Run `grunt run` or just `grunt`
